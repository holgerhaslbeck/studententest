<?php
	# Vorhandene Session starten
	session_start();
	# Session beenden
	session_destroy();
?>
<html>
<header>
	<title>Logout</title>
</header>
<body>
	<img src="/images/studentenverzeichnis.png">
	<center><font size="10">Logout</font></center>
	<br><br>
	<center>Sie sind ausgelogged.</center>
	<center><a href="/einloggen.php">Einloggen</a> - <a href="/anmelden.php">Anmelden</a></center>
</body>
</html>
