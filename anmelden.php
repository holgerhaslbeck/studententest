<?php
	#
	# Wurde das Formular abgebrochen?
	#
	if($_GET['submit']=="Abbrechen") {
		header("Location: /anmelden.php");
		exit();
	}
	#
	# URL Parameter 
	#
	$email=$_GET["email"] ?: "";
	$password=$_GET["password"] ?: "";
	$firstname=$_GET["firstname"] ?: "";
	$lastname=$_GET["lastname"] ?: "";
	$street=$_GET["street"] ?: "";
	$zip=$_GET["zip"] ?: "";
	$city=$_GET["city"] ?: "";
	$semester=$_GET["semester"] ?: "";
	#
	# Wurde das Formular abgeschickt?
	#
	if($_GET["submit"] != "") { 
		$submited=true;
	} else {
		$submited=false;
	}
	#
	# Neues PDO Objekt für die Datenbankverbindung erzeugen
	#
	$pdo = new PDO('mysql:host=127.0.0.1;dbname=student_db', 'studentdb', 'stud1pass');
	$new_student_id=-1;
	#
	# Falls alle Parameter gesetzt sind können wir den Nutzer anlegen
	#
	if(!($email == "" || $password == "" || $firstname == "" || $street  == "" || $zip  == "" || $city  == "" || $semester  == "")) {
		#
		# Benutzer in der Datenbank anlegen
		#
		$statement = $pdo->prepare("INSERT INTO students (email, firstname, lastname, street, zip, city, password) VALUES (?, ?, ?, ?, ?, ?, ?)");
		$statement->execute(array($email, $firstname, $lastname, $street, $zip, $city, $password)); 
		$new_student_id = $pdo->lastInsertId();
		if($new_student_id>0) { 
			#
			# Semester dem neuen Benutzer zuordnen
			#
			$statement = $pdo->prepare("INSERT INTO semester_students (semester_id,student_id) VALUES (?, ?)");
			$statement->execute(array($semester,$new_student_id)); 		
?>			
<html>
	<header>
		<title>Danke für die Anmeldung</title>
	</header>
	<body>
		<img src="/images/studentenverzeichnis.png">
		<center><font size="10">Danke für die Anmeldung <?php echo $neue_id; ?></font></center>
		<br><br>
		<center>Sie sind jetzt registriert und können sich einloggen!</center><br>
		<center><a href="/einloggen.php">Einloggen</a></center>
	</body>
</html>	
<?php	
		exit();
		}
	}
?>
<html>
	<header>
		<title>Anmelden</title>
	</header>
	<body>
		<img src="/images/studentenverzeichnis.png">
		<center><font size="10">Anmelden</font></center>
		<br><br>
		<form>
		Emailadresse: <input type=text name=email length=30 value="<?php echo $email; ?>"></input>
		<font color="red">
		<?php if($email == "" && $submited) { echo "Email Adresse fehlt!"; }; ?>
		<?php if($new_student_id == 0 && $submited) { echo "Account mit dieser Emailadresse existiert schon!"; }; ?>
		</font>
		<br>
		Passwort: <input type=password name=password length=30 value="<?php echo $password; ?>"></input>
		<font color="red">
		<?php if($password == "" && $submited) { echo "Email Passwort fehlt!"; } ?>
		</font>
		<br>
		Vorname: <input type=text name=firstname length=30 value="<?php echo $firstname; ?>"></input>
		<font color="red">
		<?php if($firstname == "" && $submited) { echo "Vorname fehlt!"; };  ?>
		</font>
		<br>
		Nachname: <input type=text name=lastname  length=30 value="<?php echo $lastname; ?>"></input>
		<font color="red">
		<?php if($lastname == "" && $submited) { echo "Nachname fehlt!"; }; ?>
		</font>
		<br>
		Strasse: <input type=text name=street length=30 value="<?php echo $street; ?>"></input>
		<font color="red">
		<?php if($street == "" && $submited) { echo "Strasse fehlt!"; }; ?>
		</font>
		<br>
		Postleitzahl: <input type=text name=zip length=5 value="<?php echo $zip; ?>"></input> Stadt: <input type=text name=city length=30 value="<?php echo $city; ?>"></input>
		<font color="red">
		<?php if($zip == "" && $submited) { echo "Postleitzahl fehlt!"; }; ?>
		&nbsp;
		<?php if($city == "" && $submited) { echo "Stadt fehlt!"; }; ?>
		</font>
		<br>
		Semester: <select name=semester>
			<option value="">Bitte auswählen</option>
		<?php
		#
		# Mögliche Semester aus der Datenbank laden
		#
		$get_semester_query="SELECT id,name FROM semesters";
		foreach ($pdo->query($get_semester_query) as $row) {
			if($semester==$row['id']) {
				$option_select_string=" selected";
			} else {
				$option_select_string="";
			}	
			echo "<option value=\"".$row['id']."\"".$option_select_string.">".$row['name']."</option>\n";
		}
		?>
		</select>
		<font color="red">
		<?php if($semester == "" && $submited) { echo "Bitte Semester wählen!"; }; ?>
		</font>
		<br>	
		<input type=submit name="submit" value="Abschicken"> 	<input type=submit name="submit" value="Abbrechen">
		</form>
		<br><br>
	</body>
</html>
