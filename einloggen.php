<?php
	#
	# Daten aus dem HTTP Request
	#
	$email=$_GET["email"] ?: "";
	$password=$_GET["password"] ?: "";
	#
	# Beide Daten ausgefüllt?
	#
	if($email == "" || $password == "") {
?>
<html>
<header>
	<title>Einloggen</title>
</header>
<body>
	<img src="/images/studentenverzeichnis.png">
	<center><font size="10">Einloggen</font></center>
	<br><br>
	<form>
	Emailadresse: <input type=text name=email length=30 value="<?php echo $email; ?>"></input><br>
	Passwort: <input type=password name=password length=30 value="<?php echo $password; ?>"></input><br>
	<input type=submit value="Einloggen">
	</form>
	<br><br>
	</body>
</html>
<?php
	} else {
		$error=""; 
		$user_id="";
		#
		# Neues PDO Objekt für die Datenbankverbindung erzeugen
		#
		$pdo = new PDO('mysql:host=127.0.0.1;dbname=student_db', 'studentdb', 'stud1pass');
		#
		# Nutzer in der Datenbank vorhanden?
		#
		$check_user_query="SELECT id FROM students WHERE email='$email'";
		foreach ($pdo->query($check_user_query) as $row) {
		   $results[]=$row['id'];
		}
		if(count($results)<>1) {
			$error="Benutzer existiert nicht."; 
		} else { 
			#
			# Passwort korrekt?
			#
			$user_id=$results[0];
			$check_password_query="SELECT id FROM students WHERE id='$user_id' AND password='$password'";
			$results=[];
			foreach ($pdo->query($check_password_query) as $row) {
			   $results[]=$row['id'];
			}
			if(count($results)<>1) {
				$error="Passwort ist falsch!"; 
			}
		} 
		if($error=="") {
			#
			# Neue Session starten
			#
			session_start();
			$_SESSION['user_id']=$user_id;
?>
<html>
	<header>
		<title>Eingelogged!</title>
	</header>
	<body>
		<img src="/images/studentenverzeichnis.png">
		<center><font size="10">Eingelogged</font></center>
		<br><br>
		<center>Sie sind jetzt eingelogged!</center><br>
		<center><a href="/students.php">Studenten Übersicht</a> - <a href="/edit_user.php">Eigene Daten ändern</a> -  <a href="/ausloggen.php">Ausloggen</a> </center>
	</body>
</html>		
<?php } else { ?>
<html>
	<header>
		<title>Login fehlgeschlagen</title>
	</header>
	<body>
		<img src="/images/studentenverzeichnis.png">
		<center><font size="10">Es gab ein Problem bei der Anmeldung</font></center>
		<br><br>
		<font color="red"><?php echo $error ?> </font>
		<br><br>
		<form>
		Emailadresse: <input type=text name=email length=30 value="<?php echo $email; ?>"></input><br>
		Passwort: <input type=password name=password length=30 value="<?php echo $password; ?>"></input><br>
		<input type=submit value="Einloggen">
		</form>
	</body>
</html>		
<?php
		}
	}
?>